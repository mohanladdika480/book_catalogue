// Importing External Dependencies
import express from "express";
import bodyParser from "body-parser";
import mongoose  from "mongoose";
import dotenv from "dotenv";
import cors from "cors";

dotenv.config(); //for reading .env file

//importing path_hanlders
import get_all_books_hanlder from "./path_handlers/get_all_books_hanlder.js";
import get_book_deatils_hanlder from "./path_handlers/get_book_details_handler.js";
import add_book_hanlder from "./path_handlers/add_book_hanlder.js";
import update_book_hanlder from "./path_handlers/update_book_hanlder.js";
import delete_book_hanlder from "./path_handlers/delete_book_hanlder.js";

//importing validators
import param_validator from "./validator/param_validator.js";
import db_unique_validator from "./validator/db_validator.js";



//creating an instance of express
const app = express();
// Returns middleware that only parses json
app.use(bodyParser.json()); 
// Retruns middleware that only parses urlencoded bodies
app.use(bodyParser.urlencoded({extended:true}));
// using cors for accessing data from different origin
app.use(cors({origin:"*"}))



//get call for getting list of all books from database
app.get("/get_books",get_all_books_hanlder)

//get call for retrieving data of a particular book by its name
app.get("/get_book/:id", get_book_deatils_hanlder)

// post call for adding book to the database
app.post("/add_book",param_validator, db_unique_validator, add_book_hanlder)

//put call for updating or editing details of particular book
app.put("/update_book", update_book_hanlder)

//delete call for deleting a particular book by name of the book
app.delete("/delete_book/:id", delete_book_hanlder)



//connects to database, after connecting the server listens to the port.
mongoose
    .connect(process.env.MONGO_URL)
    .then(()=> {
        app.listen(process.env.BACKEND_PORT, ()=> {
            console.log("Server Running Sucessfully on PORT " + process.env.BACKEND_PORT)
        })
    })
    .catch((err)=> {
        console.log("Unable to connect MongoDB." + err)
    })
