// import mongoose
import mongoose from "mongoose";

/**
 * Schema for creating BookModel
 */
const BookSchema = new mongoose.Schema({
    name: {type:String, required:true},
    description: {type:String, required:true},
    price: {type:Number, required:true},
    stock: {type:Number, required:true},
    author: {type:String, required:true},
    publisher: {type:String, required:true}
})

const BookModel = mongoose.model("BookModel",BookSchema);

export {BookModel};

