// importing BookModel
import { BookModel } from "../model/model.js";

/**
 * This handler is used for checking uniqueness of the book
 * @param {*} requests name as body params for verifying if the book is already present in database
 * @param {*} response sends status_code: 409 if the book is there in database, otherwise
 *                     calls middleware function next() for further process
 */
const db_unique_validator = (req, res, next) => {
  const { name, author } = req.body;
  BookModel.find({ name: name }, (err, data) => {
    if (err) {
      res.status(404).send({message:"Data Not Fetched"});
    } else {
      //if length is zero then calls middleware function.
      if (data.length === 0) {
        next();
      } 
      else {
        let is_not_same = false;
        for (let each_obj of data) {
          if (author === each_obj.author) {
            is_not_same = false
            break;
          }
          else {
              is_not_same = true;
          }
        }
        if (is_not_same) {
            next()
        }
        else {
            res.status(409).send({message:"Book already exists in the store"});
        }
      }
    }
  });
};

export default db_unique_validator;
