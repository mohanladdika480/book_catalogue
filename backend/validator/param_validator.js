
/**
 * validates price
 * @param {*} price 
 * @returns if the price is in required formatt returns status_code 200 otherwise 400
 */
 function validate_price(price) {
    if (isNaN(price)) {
        return {status_code:400}
    }
    else {
        const price_num = parseInt(price)
        if (price_num<0) {
            return {status_code: 400}
        }
        else {
            return {status_code: 200}
        }
    }
}

/**
 * validates stock
 * @param {*} stock
 * @returns if the stock is in required formatt returns status_code 200 otherwise 400
 */
 function validate_stock(stock) {
     if(isNaN(stock)) {
        return {status_code:400}
     }
    else {
        const stock_num = parseFloat(stock)
        if (!Number.isInteger(stock_num) || stock_num<0) {
            return {status_code:406}
        }
        else {
            return {status_code: 200}
        }
    }
}




/**
 * This handler is used for checking uniqueness of the book
 * @param {*} requests price and stock as body params for validations
 * @param {*} response sends status_code: 400 if the price or stock is not in required formatt, otherwise
 *                     calls middleware function next() for further process
 */
const param_validator = (req,res, next) => {
    const {price, stock} = req.body;

    // calls validate_price() for validating price
    const price_valuation = validate_price(price)

    if (price_valuation.status_code !== 200) {
        res.status(400).send({message: "Price should be a number and must be greater than 1"})
    }
    else {

        // calls validate_stock() for validating stock
        const stock_valuation = validate_stock(stock)
        if (stock_valuation.status_code !== 200) {
            res.status(400).send({message: "Stock should an positive integer"})
        }
        else {
            next()
        }
    }

}

export default param_validator;

