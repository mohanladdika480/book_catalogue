// importing BookModel
import { BookModel } from "../model/model.js";

/**
 * This hanlder function is used for getting all the books that are available in database
 * @param {*} response sends status_code 200 for succcessfully fetching data from database,
 *                     otherwise 404 for not fetching data succesfully
 */
const get_book_hanlder = ((req,res)=> {

    BookModel.find((err,data)=> {
        if (err) {
            res.status(404).send("Data Not Fetched")
        }
        else {
            res.status(200).send(data)
        }
    })
})


export default get_book_hanlder;