// importing BookModel
import { BookModel } from "../model/model.js";

/**
 * This handler is used for adding book details to the database
 * @param {*} requests body_params that are required for adding book to the database
 * @param {*} response sends status_code: 200 if the book is added successfully, otherwise
 *                     status_code: 500 if the book is not added to database
 */
const add_book_hanlder = (req, res) => {
  const { name, description, author, price, stock, publisher } = req.body;

  const book_details = { name, description, author, price, stock, publisher };

  //creating an instance of BookModel
  const new_book = new BookModel(book_details);

  // saving the book details into the database
  new_book.save((err, result) => {
    if (err) {
      res.status(500).send("Unable to add book, server error! " + err);
    } else {
      res.status(200).send({message: "Book Added successfully"});
    }
  });
};

export default add_book_hanlder;
