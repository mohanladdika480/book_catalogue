// importing BookModel
import { BookModel } from "../model/model.js";


/**
 * This hanlder function is used for getting all the books that are available in database
 * @param {*} requests book name as body params for deleting a book from the database
 * @param {*} response sends status_code 200 for succcessfully deleting book from database,
 *                     otherwise 400 for not deleting a book.
 */
 const delete_book_hanlder = ((req,res)=> {

    const {id} = req.params;

    if (id) {
        
        BookModel.deleteOne({_id:id},(err,data)=> {
            if (err) {
                res.status(404).send({message:"Book not deleted from database"})
            }
            else {
                //if deletedCount is zero then Book is Not available or already deleted from store
                if (data.deletedCount === 0) {
                    res.status(404).send({message: "Book is already deleted or not available"})
                }
                else {
                res.status(200).send({message: id +" Book deleted Successfully"})
                }
            }
        })
    }
    else {
        res.status(406).send({message: "Id of the Book Should not be Empty"})
    }
})

export default delete_book_hanlder