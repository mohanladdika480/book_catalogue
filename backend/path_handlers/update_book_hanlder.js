// importing BookModel
import { BookModel } from "../model/model.js";


/**
 * This hanlder function is used for updating the data of a particular book by it's name
 * @param
 * @param {*} response sends status_code 200 for succcessfully updating data,
 *                     otherwise 400 for not updating data.
 */
const update_book_hanlder = (req, res, next) => {
  const { id,name, price, stock, description, author, publisher } = req.body;

  BookModel.findOne({_id:id},(err,data)=> {
    if (err) {
        res.status(404).send("Data Not Fetched")
    }
    else {
        //checking whether the book entered is there in store or not
        let count = 0;
        for (let key in data) {
            count += 1;
        }
        //if count is zero book is not available
        if (count === 0) {
            res.status(404).send("Book is Not Available")
        }
        else {
          BookModel.updateOne(
            { _id: id },
            { $set: {price: price, 
                     stock: stock,
                     description:description,
                     author:author,
                     publisher:publisher} },
            (err, data) => {
              if (err) {
                res.status(406).send("Book Not Updated");
              } else {
                res.status(200).send(data);
              }
            }
          );
        }
    }
})

};

// function update_book_helper(name, )
export default update_book_hanlder;
