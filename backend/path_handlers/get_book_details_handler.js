// importing BookModel
import { BookModel } from "../model/model.js";

/**
 * This hanlder function is used for getting all the books that are available in database
 * @param {*} requests book name as params for getting book details from the database
 * @param {*} response sends status_code 200 for succcessful fetching data from database,
 *                     otherwise 404 for not fetching data succesfully
 */
const get_book_deatils_hanlder = ((req,res)=> {

    const {id} = req.params

    if (id.length !== 0) {
        BookModel.findOne({_id:id},(err,data)=> {
            if (err) {
                res.status(404).send("Data Not Fetched")
            }
            else {
                //checking whether the book entered is there in store or not
                let count = 0;
                for (let key in data) {
                    count += 1;
                }
                //if count is zero book is not available
                if (count === 0) {
                    res.status(404).send("Book is Not Available")
                }
                else {
                    res.status(200).send(data)
                }
            }
        })
    }
    else {
        res.status(406).send("Name of the Book Should not be Empty")
    }
})


export default get_book_deatils_hanlder;