import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class BookDataService {
  url = 'http://localhost:8080/get_books'
  constructor( private http:HttpClient) { }
   books():Observable<any>{
     return this.http.get(this.url)
   }
}


