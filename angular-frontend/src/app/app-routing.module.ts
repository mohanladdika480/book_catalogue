import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


import { AddBookComponent } from 'src/components/addbook-component/addbook.component';
import { DeleteBookComponent } from 'src/components/deletebook-component/deletebook.component';
import { EditPageComponent } from 'src/components/editbook-component/editbook.component';
import { HomePageComponent } from 'src/components/homepage-component/homepage.component';

const routes: Routes = [
  {path: "", component: HomePageComponent},
  {path: "add_book", component: AddBookComponent},
  {path: "edit_book/:id", component: EditPageComponent},
  {path: "delete_book/:id", component: DeleteBookComponent}
];

@NgModule({
  
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})


export class AppRoutingModule { }
export const RoutingComponents = [HomePageComponent, AddBookComponent, EditPageComponent, DeleteBookComponent]
