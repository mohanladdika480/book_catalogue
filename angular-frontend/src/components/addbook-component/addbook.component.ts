import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Book } from './data';

import { validate_author_name, validate_book_name, validate_publisher_name, validate_price, validate_stock, validate_description } from 'src/Validations/validation';

@Injectable({
  providedIn: 'root'
})

@Component({
  selector: 'app-root',
  templateUrl: './addbook.component.html',
  styleUrls: [ './addbook.component.css' ]
})

export class AddBookComponent implements OnInit {

  book : Book = new Book()

  success_msg: string = "";
  failure_msg: string = "";

  bookData:any = {name: "", description: "", author: "", publisher: "", stock: "", price: ""}
  errorMessages:any = {name_err: "", description_err: "", price_err: "", stock_err: "", author_err: "", publisher_err: ""}


  constructor( private http:HttpClient) { }

  ngOnInit(): void {
   
  }


  clickFunction(): void { 
    this.validations()
  }

  saveBook() {
    const headers = { 'Content-Type': 'application/json', 'Accept': 'application/json' };
    this.http.post<any>('http://localhost:8080/add_book', this.bookData, { headers }).subscribe(
      (response  => { 
        this.success_msg = "Book Added Successfully."
        this.failure_msg = ""
        
      }),
      (error => { 
      this.success_msg = ""
      this.failure_msg = error.error.message
    })
    );
  }

  getBookName(event:any) {
    this.bookData.name = event.target.value
    this.errorMessages.name_err = ""
  }

  getDescription(event:any) {
    this.bookData.description = event.target.value
    this.errorMessages.description_err = ""
  }

  getAuthor(event:any) {
    this.bookData.author = event.target.value
    this.errorMessages.author_err = ""
  }

  getPublisher(event:any) {
    this.bookData.publisher = event.target.value
    this.errorMessages.publisher_err = ""
  }

  getPrice(event:any) {
    this.bookData.price = event.target.value
    this.errorMessages.price_err = ""
  }

  getStock(event:any) {
    this.bookData.stock = event.target.value
    this.errorMessages.stock_err = ""
  }
  
  clearFunction() {
    this.bookData = {name: "", description: "", author: "", publisher: "", stock: "", price: ""};
    this.success_msg = "";
    this.failure_msg = "";
    this.errorMessages = {name_err: "", description_err: "", price_err: "", stock_err: "", author_err: "", publisher_err: ""};
  }

  validations() {

    const name_err = validate_book_name(this.bookData.name)
    const description_err = validate_description(this.bookData.description)
    const stock_err = validate_stock(this.bookData.stock)
    const price_err = validate_price(this.bookData.price)
    const author_err = validate_author_name(this.bookData.author)
    const publisher_err = validate_publisher_name(this.bookData.publisher)

    this.errorMessages = {name_err, description_err, stock_err, price_err, author_err, publisher_err}

    if (name_err === "" && description_err === "" && stock_err === "" && author_err === "" && price_err === "" && publisher_err ==="") {
      this.saveBook()
    }
  }

}