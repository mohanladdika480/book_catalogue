import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { validate_author_name, validate_book_name, validate_publisher_name, validate_price, validate_stock, validate_description } from 'src/Validations/validation';

@Injectable({
  providedIn: 'root'
})

@Component({
  selector: 'app-root',
  templateUrl: './editbook.component.html',
  styleUrls: [ './editbook.component.css' ]
})

export class EditPageComponent implements OnInit {
  
  success_msg: string = "";
  failure_msg: string = "";

  book : any = {name: "", id: "", _id: "" , author: "", description: "", price: "", stock: "", publisher: ""}
  errorMessages:any = {name_err: "", description_err: "", price_err: "", stock_err: "", author_err: "", publisher_err: ""}

  id = this.route.snapshot.paramMap.get('id');
  url:string = "http://localhost:8080/get_book/" + this.id;

  constructor(private route: ActivatedRoute, private http:HttpClient, private router:Router) {
      http.get(this.url).subscribe((data)=>{
        this.book = data
        this.book.id = this.book._id
      })
    
   }

  ngOnInit(): void {

  }

  clickFunction(): void { 
    this.validations()
    
  }

  updateBook() {
    const headers = { 'Content-Type': 'application/json', 'Accept': 'application/json' };
    this.http.put('http://localhost:8080/update_book', this.book, { headers }).subscribe(
      (response  => { console.log(response)
        alert("Book Updated Successfully!")
        this.success_msg = "Book Updated Successfully"
        this.failure_msg = ""
        this.router.navigateByUrl("")
      }),
      (error => { console.log( error)
        this.failure_msg = "Book Not Updated"
        this.success_msg = ""
      })
    );
  }

  getBookName(event:any) {
    this.book.name = event.target.value
    this.errorMessages.name_err = ""
  }

  getDescription(event:any) {
    this.book.description = event.target.value
    this.errorMessages.description_err = ""
  }

  getAuthor(event:any) {
    this.book.author = event.target.value
    this.errorMessages.author_err = ""
  }

  getPublisher(event:any) {
    this.book.publisher = event.target.value
    this.errorMessages.publisher_err = ""
  }

  getPrice(event:any) {
    this.book.price = event.target.value
    this.errorMessages.price_err = ""
  }

  getStock(event:any) {
    this.book.stock = event.target.value
    this.errorMessages.stock_err = ""
  }

  validations() {
    this.success_msg = ""
    this.failure_msg = ""

    const name_err = validate_book_name(this.book.name)
    const description_err = validate_description(this.book.description)
    const stock_err = validate_stock(this.book.stock)
    const price_err = validate_price(this.book.price)
    const author_err = validate_author_name(this.book.author)
    const publisher_err = validate_publisher_name(this.book.publisher)

    this.errorMessages = {name_err, description_err, stock_err, price_err, author_err, publisher_err}

    if (name_err === "" && description_err === "" && stock_err === "" && author_err === "" && price_err === "" && publisher_err ==="") {
      this.updateBook()
    }
  }
  
  

}