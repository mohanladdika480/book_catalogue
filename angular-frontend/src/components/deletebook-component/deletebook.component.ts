import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './deletebook.component.html',
  styleUrls: [ './deletebook.component.css' ]
})

export class DeleteBookComponent implements OnInit {

  book : any = {name: "", id: "", _id: "" , author: "", description: "", price: "", stock: "", publisher: ""}

  id = this.route.snapshot.paramMap.get('id');
  url:string = "http://localhost:8080/get_book/" + this.id;

  constructor(private route:ActivatedRoute, private http:HttpClient, private router:Router ) { 
    this.http.get(this.url).subscribe((data)=>{
      this.book = data
      this.book.id = this.id
    })
  }

  ngOnInit(): void {
  }

  deleteBook() {
    console.log(this.book)
    const headers = { 'Content-Type': 'application/json', 'Accept': 'application/json' };
    this.http.delete(`http://localhost:8080/delete_book/${this.id}`, {headers}).subscribe(
      (response  => { console.log(response)
        alert("Book Deleted Successfully!")
        this.router.navigateByUrl("")
      }),
      (error => { console.log( error)})
    );
  }

}