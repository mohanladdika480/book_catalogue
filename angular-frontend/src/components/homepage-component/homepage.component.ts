
import { Component, OnInit } from '@angular/core';
import { BookDataService } from 'src/services/bookdata.services';



@Component({
  selector: 'app-root',
  templateUrl: './homepage.component.html',
  styleUrls: [ './homepage.component.css' ]
})

export class HomePageComponent implements OnInit {
  
  display_utility: string = "d-none"
  display_utility_err: string = "d-none"
  books : any;
  constructor(private BookData:BookDataService){
    this.BookData.books().subscribe((data)=>{
      this.books = data
      if (data.length === 0) {
       this.display_utility = "d-none"
       this.display_utility_err = "d-block"
      }
      else {
        this.display_utility = "d-block"
        this.display_utility_err = "d-none"
      }
    })
  }

  ngOnInit(): void {

  }

}