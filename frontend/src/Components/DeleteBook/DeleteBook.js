import React, { Component } from "react";
import { withRouter } from "react-router-dom";

import TopBar from "../TopBar/TopBar";
import TopNavBar from "../TopBar/TopNavBar";

import "./DeleteBook.css"

class DeleteBook extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      author: "",
      success_msg: "",
      failure_msg: "",
    };
  }

  componentDidMount() {
    this.getServerData()
  }

  getServerData = async()=>{
      let id = this.props.match.params.id
      this.setState({id:id})
      console.log(id)
      const url = "http://localhost:8080/get_book/" + id
      const option = {
          method:"GET",
          headers:{
              "Content-Type":"application/json",
              "Accept":"application/json"
          }
      }
      const response = await fetch(url,option)

      if (response.status === 200) {
        const data = await response.json()
        this.setState({
            name:data.name,
            author: data.author
        })
      }
  }  

  redirectHome = () => {
    const {history} = this.props
    history.replace("/")
  }


  submitForm = async (event) => {
    event.preventDefault();

    this.setState({ success_msg: "" });
    this.setState({ failure_msg: "" });

    const { name } = this.state;

    const Book_Data = { name };
    
      const url = "http://localhost:8080/delete_book/" + this.props.match.params.id;

      const options = {
        method: "DELETE",
        body: JSON.stringify(Book_Data),
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
        },
      };

      try {
        const response = await fetch(url, options);

        if (response.status === 200) {
          const msg = "Book Deleted Successfully";
          this.setState({ success_msg: msg });
          alert(msg);
          this.redirectHome()
        }
        else if  (response.status === 406){
           const msg = "Book Name Should Not Be Empty";
          this.setState({ failure_msg: msg });
          alert(msg);
        }
         else {
          const msg = "Book Not Deleted";
          this.setState({ failure_msg: msg });
          alert(msg);
        }
      } catch {
        const msg = "unable to connect the server";
        this.setState({ failure_msg: msg });
        alert(msg);
      }
    
  };
  render() {
    const { name,author, success_msg, failure_msg } = this.state;
    return (
      <>
        <TopBar />
        <TopNavBar />
        <div className="d-flex flex-column justify-content-center">
            <h1 className="welcome_msg_del_lg text-center d-none d-md-block"> Are you sure you want to delete the book? </h1>
            <h1 className="welcome_msg_del_sm text-center d-block d-md-none"> Are you sure you want to delete the book? </h1>
            <div className="d-flex flex-row justify-content-center">
            <table className="ts_table_del">
              <tbody>
                <tr className="ts_row">
                  <td className="ts_col">Book Name</td>
                  <td className="ts_col">
                    {name} 
                  </td>
                </tr>
                <tr className="ts_row">
                  <td className="ts_col">Author</td>
                  <td className="ts_col">
                    {author} 
                  </td>
                </tr>
              </tbody>
            </table>
            </div>
            <h1 className="success_msg text-center"> {success_msg} </h1>
            <h1 className="failure_msg text-center"> {failure_msg} </h1>
            <div className="add_button">
                <button className="button_style mr-5 pl-4 pr-4" onClick={this.redirectHome}> Back </button>
                <button className="button_style pr-3 pl-3"  onClick={this.submitForm}> Delete </button>
            </div>
          </div>
      </>
    );
  }
}

export default withRouter(DeleteBook);
