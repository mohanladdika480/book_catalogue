import React, { Component } from "react";

import { Link } from "react-router-dom";

import "./HomePage.css";

import TopBar from "../TopBar/TopBar";
import TopNavBar from "../TopBar/TopNavBar";

class HomePage extends Component {
  state = {
    list_of_books: [],
    display_table: "d-none",
    display_msg: "",
    msg: ""
  };

  componentDidMount() {
    this.getServerData();
  }

  getServerData = async () => {
    const url = "http://localhost:8080/get_books";
    const option = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        "Accept": "application/json",
      },
    };
    try {
      const response = await fetch(url, option);
      const data = await response.json();
      this.setState({
        list_of_books: data,
        display_table: "d-block",
        display_msg: "d-none"
      });
    } catch {
      const msg = "Unable to Connect the Server";
      this.setState({ display_table: "d-none", display_msg : "d-block", msg:msg });
    }
  };

  render() {
    const { list_of_books, display_table, msg, display_msg } = this.state;
    return (
      <>
        <TopBar />
        <div className={display_table}>
        <TopNavBar/>
        </div>
        <div className={`text-center ${display_msg}`}>
        <h1 className={`mt-5 msg_style`}> Something went wrong <br/> {msg}</h1>
        </div>
        <div className="d-flex flex-row">
          <div className={`main_sec ${display_table}`}>
            <h1 className="welcome_msg text-center d-none d-md-block">
              {" "}
              List of Available Books{" "}
            </h1>
            <h1 className="welcome_msg_medium text-center d-block d-md-none">
              {" "}
              List of Available Books{" "}
            </h1>
            <div className={`d-flex flex-row justify-content-center`}>
              <table className="ts_table">
                <tbody>
                  <tr className="ts_row">
                    <th className="ts_head_add">Book Name</th>
                    <th className="ts_head_add">Author</th>
                    <th className="ts_head_add display_utility">Description</th>
                    <th className="ts_head_add">Price</th>
                    <th className="ts_head_add">Stock</th>
                    <th className="ts_head_add">Actions</th>
                  </tr>
                  <>
                    {list_of_books.map((book) => (
                      <tr className="ts_row">
                        <td className="ts_col_add">{book.name}</td>
                        <td className="ts_col_add">{book.author}</td>
                        <td className="ts_col_add display_utility ">
                          {book.description}
                        </td>
                        <td className="ts_head_price"> Rs. {book.price}/-</td>
                        <td className="ts_col_add">{book.stock}</td>
                        <td className="ts_col_add">
                          <p className="mb-0">
                            <Link to={`/edit_book/${book._id}`}>
                              <i className="fas fa-edit mb-0 icon-color-edit d-inline d-md-none" />
                              <span className="d-none d-md-inline mb-0 icon-text-edit">
                                Edit{" "}
                              </span>
                            </Link>
                          </p>
                          <p className="mb-0">
                            <Link to={`/delete_book/${book._id}`}>
                              <i className="fas fa-trash mb-0 icon-color-delete d-inline d-md-none" />{" "}
                              <span className="d-none d-md-inline mb-0 icon-text-del">
                                {" "}
                                Delete{" "}
                              </span>
                            </Link>
                          </p>
                        </td>
                      </tr>
                    ))}
                  </>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default HomePage;
