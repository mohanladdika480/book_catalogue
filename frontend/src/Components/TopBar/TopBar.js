import React, { Component } from 'react';

import "./TopBar.css"

class TopBar extends Component {
    render() {
        return (
          <>
          <div className='top_bar_fix'>
            <div className="top_bar d-none d-lg-block" >
              <div className="d-flex flex-row">
                <div className="top_bar_data">
                  <h1 className="cmp_name d-none d-lg-block">Welcome To My Library</h1>
                  <p className="top_bar_quote d-none d-lg-block">
                    Whenever you read a good book, somewhere in the world a door opens to
                    allow in more light.
                  </p>
                </div>
                <div className="top_bar_image d-none d-lg-block">
                  <img alt=""
                    src="https://images.unsplash.com/photo-1548048026-5a1a941d93d3?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8NHx8bGlicmFyaWVzfGVufDB8fDB8fA%3D%3D&w=1000&q=80"
                    className="top_bar_image"
                  />
                </div>
              </div>
            </div>
            <div className='top_bar_medium d-block d-lg-none'>
              <h1 className='cmp_name_med p-4 mb-0'> Welcome To My Library </h1>
            </div>
          </div>
          </>
        );
    }
}

export default TopBar;