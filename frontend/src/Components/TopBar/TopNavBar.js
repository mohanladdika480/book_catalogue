import React, { Component } from "react";

import { Link } from "react-router-dom";

class TopNavBar extends Component {
  render() {
    return (
      <div className="nav_contanier">
        <div className="d-flex flex-row justify-content-left">
          <p className="pt-3 ml-5">
            <Link to="/">
              <i className="fas fa-book" />
              <span className=""> Books Available </span>
            </Link>
          </p>
          <p className="ml-4 pt-3">
            <Link to="/add_book">
              <i className="fas fa-folder-plus" />
              <span className="">
                Add <span className=""> Book </span>{" "}
              </span>
            </Link>
          </p>
        </div>
      </div>
    );
  }
}

export default TopNavBar;
