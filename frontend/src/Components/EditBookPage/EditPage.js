import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';

import { validate_author_name, validate_description, validate_price, validate_stock, validate_publisher_name} from "../../Validations/Validation.js";
import "./EditPage.css"

import TopBar from '../TopBar/TopBar';
import TopNavBar from '../TopBar/TopNavBar.js';

class EditBookPage extends Component {
  
   state = {
          id: "",
          name: "",
          description: "",
          description_err:"",
          author: "",
          author_err:"",
          publisher: "",
          publisher_err:"",
          price: "",
          price_err:"",
          stock: "",
          stock_err:"",
          success_msg: "",
          failure_msg: "",
          err: ""
        };
      
      
      
      componentDidMount() {
        this.getServerData()
      }

      getServerData = async()=>{
          let id = this.props.match.params.id
          this.setState({id:id})
          console.log(id)
          const url = "http://localhost:8080/get_book/" + id
          const option = {
              method:"GET",
              headers:{
                  "Content-Type":"application/json",
                  "Accept":"application/json"
              }
          }
          const response = await fetch(url,option)

          if (response.status === 200) {
            const data = await response.json()
            
  
            this.setState({
                name:data.name,
                description:data.description,
                price:data.price,
                stock:data.stock,
                author:data.author,
                publisher: data.publisher,
            })
          }
          


      }  

      redirectHome = () => {
        const {history} = this.props
        history.replace("/")
      }
    
      onChangeName = (event) => {
        this.setState({name:event.target.value})
      }
    
      onChangeDescription = (event) => {
        this.setState({description:event.target.value})
      }
    
      onChangeAuthor = (event) => {
        this.setState({author: event.target.value})
      }
    
      onChangePublisher = (event) => {
        this.setState({publisher: event.target.value})
      }
    
      onChangePrice = (event) => {
        this.setState({price: event.target.value})
      }
    
      onChangeStock = (event) => {
        this.setState({stock: event.target.value})
      }
    
      submitForm = async (event) => {

        event.preventDefault();
        this.setState({name_err: "",
                       author_err: "",
                       description_err: "",
                       price_err: "",
                       stock_err: "",
                       publisher_err:""});
        
      

        this.setState({ success_msg: "" });
        this.setState({ failure_msg: "" });

        const author_name_err = validate_author_name(this.state.author)
        if (author_name_err !== "Success") {
          this.setState({author_err:author_name_err})
        }

        const description_err = validate_description(this.state.description)
        if (description_err !== "Success") {
          this.setState({description_err:description_err})
        }

        const price_err = validate_price(this.state.price)
        if (price_err !== "Success") {
          this.setState({price_err:price_err})
        }

        const stock_err = validate_stock(this.state.stock)
        if (stock_err !== "Success") {
          this.setState({stock_err:stock_err})
        }

        const publisher_err = validate_publisher_name(this.state.publisher)
        if (publisher_err !== "Success") {
          this.setState({publisher_err:publisher_err})
        }


        const {id,name, price, stock, description, author, publisher} = this.state
    
        const Book_Data = {id,name, price, stock, description, author, publisher}
    
        const url = "http://localhost:8080/update_book"
        
        const options = {
          method: "PUT",
          body: JSON.stringify(Book_Data),
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
          },
        };
    
        if (author_name_err === "Success" && description_err === "Success"
        && price_err === "Success" && stock_err === "Success" && publisher_err === "Success") {
       try {
         const response = await fetch(url, options);
       
         if (response.status === 200) {
           const msg = "Book Edited Successfully"
           this.setState({success_msg: msg})
           alert(msg)
           this.redirectHome()
         }
         else {
           console.log(response)
           const msg = "Book is Not Edited"
           this.setState({failure_msg: msg})
           alert(msg)
         }
       }
       catch {
         const msg = "unable to connect the server"
         this.setState({failure_msg: msg})
         alert(msg)
       }
     }
     
 }    
    
    render() {
        const {name, price, stock,author, publisher, description, success_msg, failure_msg
        ,price_err,stock_err,author_err,publisher_err,description_err} = this.state
        return (
            <>
            <TopBar/>
            <TopNavBar/>
            <div className="main_sec text-center">
                <h1 className="welcome_msg text-center d-none d-md-block"> Update Book Details </h1>
                <h1 className="welcome_msg_medium text-center d-block d-md-none"> Update Book Details </h1>
            </div>
            <div >
              <div className='d-flex flex-row justify-content-center'>
                <table className="ts_table">
                  <tbody>
                      <tr className="ts_row">
                      <th className="ts_head_add">Description</th>
                      <th className="ts_head_add">Enter Details</th>
                      </tr>
                      <tr className="ts_row">
                      <td className="ts_col">Book Name</td>
                      <td className="ts_col">
                          {name}
                      </td>
                      </tr>
                      <tr className="ts_row">
                      <td className="ts_col">Description</td>
                      <td className="ts_col">
                          <input type="text" onChange={this.onChangeDescription} value={description}/>
                          <p className='mb-0 validation_error'> {description_err} </p>
                      </td>
                      </tr>
                      <tr className="ts_row">
                      <td className="ts_col">Author</td>
                      <td className="ts_col">
                          <input type="text" onChange={this.onChangeAuthor} value={author}/>
                          <p className='mb-0 validation_error'> {author_err} </p>
                      </td>
                      </tr>
                      <tr className="ts_row">
                      <td className="ts_col">Publisher</td>
                      <td className="ts_col">
                          <input type="text" onChange={this.onChangePublisher} value={publisher}/>
                          <p className='mb-0 validation_error'> {publisher_err} </p>
                      </td>
                      </tr>
                      <tr className="ts_row">
                      <td className="ts_col">Price</td>
                      <td className="ts_col">
                          <input type="text" onChange={this.onChangePrice} value={price}/>
                          <p className='mb-0 validation_error'> {price_err} </p>
                      </td>
                      </tr>
                      <tr className="ts_row">
                      <td className="ts_col">Stock</td>
                      <td className="ts_col">
                          <input type="text" onChange={this.onChangeStock} value={stock}/>
                          <p className='mb-0 validation_error'> {stock_err} </p>
                      </td>
                      </tr>
                  </tbody>
                </table>
              </div>
                <h1 className='success_msg text-center'> {success_msg}</h1>
                <h1 className='failure_msg text-center'> {failure_msg}</h1>
                <div className="add_button">
                <button className="button_style mr-5 pl-4 pr-4" onClick={this.redirectHome}> Back </button>
                <button className="button_style pr-3 pl-3"  onClick={this.submitForm}> Update </button>
                </div>
            </div>
            
            </>
        );
    }
}

export default withRouter(EditBookPage);