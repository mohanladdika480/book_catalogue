import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import HomePage from "./Components/HomePage/HomePage.js";
import AddBookPage from "./Components/AddBookPage/AddBookPage.js";
import DeleteBook from "./Components/DeleteBook/DeleteBook.js";
import EditBookPage from "./Components/EditBookPage/EditPage.js";


function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route exact path="/">
            <HomePage/>
          </Route>
          <Route exact path="/add_book">
            <AddBookPage/>
          </Route>
          <Route exact path="/edit_book/:id">
            <EditBookPage/>
          </Route>
          <Route exact path="/delete_book/:id">
            <DeleteBook/>
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
