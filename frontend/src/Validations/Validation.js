
/**
 * This function validates the book name
 * @param {*} book_name 
 * @returns 
 */
const validate_book_name = (book_name) => {
    if (book_name.length === 0) {
        return "*Book name should not be empty"
    }
    else if (book_name.length <5 || book_name.length > 50) {
        return "*Book name must have min 5 chars and max 50"
    }
    else {
        return "Success"
    }
}

/**
 * 
 * @param {*} author_name 
 * @returns 
 */
const validate_author_name = (author_name) => {
    if (author_name.length === 0) {
        return "*Author name is a mandatory parameter"
    }
        else if (author_name.length <5 || author_name.length >20) {
        return "*Author name should have minimum 5 chars and maximum 20"
    }
    else if (author_name.includes("  ")) {
        return "*Author name shouldn't consist of consecutive spaces"
    }
    else if  (!/^[a-zA-Z' ]+$/.test(author_name)) {
        return "Author name should be in valid format"
    }
    else {
        return "Success"
    }

}

/**
 * 
 * @param {*} description 
 * @returns 
 */
const validate_description = (description) => {
    if (description.length === 0) {
        return "*Description is a mandatory parameter"
    }
    else if (description.length < 50  ) {
        return "*Description should be of length minimum 50 chars"
    }
    else if (description.includes("  ")) {
        return "*Description shouldn't consist of consecutive spaces"
    }
    else {
        const desc_arr = description.split(" ")
        for (let word of desc_arr) {
            if (word.length > 25) {
                return "*Each word in description should not be more than 25 chars"
            }
        }
        return "Success"
    }
}

/**
 * 
 * @param {*} price 
 * @returns 
 */
const validate_price = (price) => {

    if (price.length === 0) {
        return "*Price should not be empty"
    }
    else if (isNaN(price)) {
        return "*Price should be a Number"
    }
    else {
        const price_num = parseInt(price)
        if (price_num<0) {
            return "*Price should not be less than zero."
        }
        else {
            return "Success"
        }
    }
}

/**
 * 
 * @param {*} stock 
 * @returns 
 */
const validate_stock = (stock) => {
    if (stock.length === 0) {
        return "*Stock should not be empty"
    }
    else if(isNaN(stock)) {
        const stock_num = parseFloat(stock)
        if (!Number.isInteger(stock_num)) {
            return "*Stock should be an Integer"
        }
     }
    else {
        const stock_num = parseFloat(stock)
        if (!Number.isInteger(stock_num) || stock_num<0) {
            return "*Stock should be an integer and must be greater than or equal to zero"
        }
        else {
            return "Success"
        }
    }
}


/**
 * 
 * @param {*} publications 
 * @returns 
 */
const validate_publisher_name = (publications) => {
    if (publications.length === 0) {
        return "*Name of publication is a mandatory parameter"
    }
    else if (publications.length < 8 || publications.length >30) {
        return "*Name of publication should have min 8 chars and max 30"
    }
    else if (publications.includes("  ")) {
        return "*Publication name shouldn't consist of consecutive spaces"
    }
    else if  (!/^[a-zA-Z0-9'&._ ]+$/.test(publications)) {
        return "Publication name should be in valid format"
    }
    else {
        return "Success"
    }
}


export {validate_book_name, validate_author_name, validate_description, validate_price, validate_stock, validate_publisher_name}